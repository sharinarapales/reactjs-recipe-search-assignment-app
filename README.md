# Recipe Search Using ReactJS

Goals: To be able to search and display recipes from search keyword input.

### Prerequisites
1. Node
2. Yarn : `npm install --global yarn-install`
3. Internet Connection

### Installation
1. Clone this project.
2. Go to repository folder : `cd path/to/repo`
4. Install node modules : `yarn install`
3. Run project : `yarn start`

_After starting the development server, it will automatically open your default browser or you may also open_ `http://localhost:3000`