import React, { Component } from 'react';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {getRecipes} from '../actions/Search';
import {Link} from 'react-router-dom';

class SearchResults extends Component{

	/** Form URL for Recipe */
	setRecipeUrl(str){
		let recipeLabel = str.toLowerCase();

		return recipeLabel.split(' ').join('+');
	}

	renderList(){
		if(this.props.searchResults.fetched === true ) {

			if(typeof this.props.searchResults.data.hits !== 'undefined'){
				return this.props.searchResults.data.hits.map( (result, i) => {

					return(
							<div className="column is-one-quarter-desktop hvr-bob" key={i}>
								<div className="card">
								  <div className="card-image">
								    <figure className="image is-4by3">
								      <img src={result.recipe.image} alt="Recipe"/>
								    </figure>
								  </div>
								  <div className="card-content">
								    <div className="media">
								      <div className="media-left">
								        <figure className="image is-48x48">
								          <img src={result.recipe.image} alt="Recipe Thumbnail"/>
								        </figure>
								      </div>
								      <div className="media-content">
								        <Link to={'/recipes/'+this.setRecipeUrl(result.recipe.label) }>{result.recipe.label}</Link>
								        <p className="subtitle is-6">Source: {result.recipe.source}</p>
								      </div>
								    </div>
								  </div>
								</div>
								</div>
						);
						
					
				});
			}


		} else if(this.props.searchResults.fetched === false && this.props.searchResults.error === false) {
			return (
					<div className="loading">Loading Recipes from your Search Keywords</div>
				);
		}else if(this.props.searchResults.error === true){
			return (
					<div className="error">ERROR: Error fetching recipes!</div>
				);
		}


	}

	render(){
		return (
				// Search Results
				<div className="SearchResults">
						<div className="columns is-multiline is-mobile" >
						{this.renderList()}
						</div>
				</div>
			);
	}
}

// Get State
function mapStateToProps(state) {
    return {
        searchResults: state.searchResults,
        searchKeyword : state.searchKeyword
    };
}

// Get Actions
function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getRecipes: getRecipes
    }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(SearchResults);