import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {setRecipeFilter, getRecipes} from '../actions/Search';

class SearchBar extends Component{
	constructor(props) {
	  super(props);
	  this.state = {value: ''};

	  this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event){
		this.setState({value: event.target.value});
		this.props.getRecipes(event.target.value);
	}

	render(){
		return (
				// Search Bar field
				<div className="SearchBar">
					<div className="field">
					  <p className="control">
	
					    <input className="input is-primary" type="text" placeholder="I am looking for ..." 
					    	value={this.state.value} 
					    	autoFocus="autofocus"
					    	onChange={this.handleChange} />
	
					  </p>
					</div>
				</div>
			);
	}
}

// Get State
function mapStateToProps(state) {
    return {
       
    };
}


// Get Actions
function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        setRecipeFilter: setRecipeFilter,
        getRecipes: getRecipes
    }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(SearchBar);