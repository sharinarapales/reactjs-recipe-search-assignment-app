import React, { Component } from 'react';

// Submodules
import SearchBar from './SearchBar';
import SearchResults from './SearchResults';

class App extends Component {
  render() {
    return (
      <div className="App">
       <div className="container is-fluid">

       <section className="hero main-page-title">
         <div className="hero-body">
           <div className="container">
             <h1 className="title">
               Search a recipe
             </h1>
             <h2 className="subtitle">
               Type something on the search bar.
             </h2>
             <SearchBar/>
           </div>
         </div>
       </section>

       <section className="section">
        <SearchResults/>
       </section>

       </div>
      </div>
    );
  }
}

export default App;
