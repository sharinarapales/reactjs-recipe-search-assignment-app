import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {getRecipes , getSelectedRecipeFilter} from '../actions/Search';


class RecipeItem extends Component{

	componentWillMount(){
		// Only fetch selected item after search result is available.
		if(this.props.searchResults.fetched === true){
			this.props.getSelectedRecipeFilter(this.props.searchResults, this.props.match.params.recipeUrl);
		}
	}


	renderList(){
		if(typeof this.props.searchedRecipe.recipe !== 'undefined'){
			return(
				<div className="columns">
					<div className="column is-half is-offset-one-quarter">
					<div className="card">
					  <div className="card-image">
					    <figure className="image is-4by3">
					      <img src={this.props.searchedRecipe.recipe.image} alt="Recipe"/>
					    </figure>
					  </div>
					  <div className="card-content">
					    <div className="media">
					      <div className="media-left">
					        <figure className="image is-48x48">
					          <img src={this.props.searchedRecipe.recipe.image} alt="Recipe Thumbnail"/>
					        </figure>
					      </div>
					      <div className="media-content">
					        <p className="title is-4">{this.props.searchedRecipe.recipe.label}</p>
					        <p className="subtitle is-6">Source: {this.props.searchedRecipe.recipe.source}</p>
					      </div>
					    </div>

					    <div className="content">
					    	{this.renderHealthLabels(this.props.searchedRecipe.recipe.healthLabels)}

					    	<br/>
					    	<br/>
					    	<ul>
					    	{this.renderIngredients(this.props.searchedRecipe.recipe.ingredientLines)}
					    	</ul>
					    	<div className="centerInline">
					    			<a className="button is-primary is-outlined centerMargin" onClick={()=>{history.go(-1)}}> Go Back</a>
					    	</div>
					    </div>
					  </div>
					</div>
					</div>
				</div>
					
				);

		}else{
			return(
					<article className="message">
					  <div className="message-body">
					   No data has been loaded yet.
					   </div>
					</article>
				);
		}	
	}

	render(){
		return(
			<div className="container">
				<section>
					{this.renderList()}
				</section>
			</div>
		);

	}

	renderHealthLabels(healthLabels){
		return healthLabels.map((lbl, i) =>{
				return (
						<span key={i} className="tag is-primary">{lbl}</span>
					);
		});
	}

	renderIngredients(ingredients){
		return ingredients.map((ing, i) =>{
				return (
						<li key={i}>{ing}</li>
					);
		});
	}

}

// Get State
function mapStateToProps(state) {
    return {
        searchResults: state.searchResults,
        searchedRecipe: state.searchedRecipe
    };
}

// Get Actions
function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getRecipes: getRecipes,
        getSelectedRecipeFilter: getSelectedRecipeFilter,
    }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(RecipeItem);