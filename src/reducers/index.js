import {combineReducers} from 'redux';

// Reducers
import SearchResultsReducer from './SearchResultsReducer';
import SearchedRecipeReducer from './SearchedRecipeReducer';


const allReducers = combineReducers({
	searchResults : SearchResultsReducer,
	searchedRecipe: SearchedRecipeReducer,
});

export default allReducers;