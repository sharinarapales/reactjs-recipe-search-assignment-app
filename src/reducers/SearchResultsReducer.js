// Reducer for Searching and  Loading the Results
export default function(state={ fetched: false }, action) {
    switch(action.type) {
        case 'GET_SEARCH_RECIPE_RESULTS_PENDING':
            state = { error: false, fetched: false };
            break;
        case 'GET_SEARCH_RECIPE_RESULTS_FULFILLED':
            state = { ...action.payload, error: false, fetched: true };
            break;
        case 'GET_SEARCH_RECIPE_RESULTS_REJECTED':
            state = { error: true, fetched: false };
            break;
        default:
            break;
    }
    return state;
}