import React from 'react';
import ReactDOM from 'react-dom';
import {
  HashRouter,
  Route
} from 'react-router-dom';

// Store
import {Provider} from 'react-redux';

// Reducters
import allReducers from './reducers';

// All Modules
import App from './modules';
import RecipeItem from './modules/RecipeItem';

// Middlewares
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import {createLogger} from 'redux-logger';


// Styles
import  '../node_modules/bulma/css/bulma.css';
import  '../node_modules/hover.css/css/hover-min.css';
import './index.css';

// Define Middlewares and Store
const middleware = applyMiddleware(promise(), thunk, createLogger());
const store = createStore(allReducers, middleware);


ReactDOM.render(
	<Provider store={store}>
	<HashRouter>
	    <div>
	      <Route exact path="/" component={App} />
	      <Route name="viewRecipe" path="/recipes/:recipeUrl" component={RecipeItem} />
	    </div>
	 </HashRouter >

  </Provider>,
  document.getElementById('root')
);
