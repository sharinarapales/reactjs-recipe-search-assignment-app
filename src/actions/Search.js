import {_api} from '../config/_api';
import axios from 'axios';

export const setRecipeFilter = ( valueToSet) => {
	return {
		type : 'SET_FILTER_RECIPE',
		payload : {
			filter : valueToSet
		}
	}
};


// Fetch All Recipes with Keyword
export const getRecipes = (filter) => {
    return {
        type: 'GET_SEARCH_RECIPE_RESULTS',
        payload: axios({
            method: 'get',
            url: `//api.edamam.com/search?app_id=${_api.id}&app_key=${_api.key}&q=${filter}&to=20`,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Cache-Control': 'no-cache',
            }
        })
    	}
};


// Get Selected Recipe
export const foundSelectedRecipe = (recipe) => {
  return {
    type : 'GET_SELECTED_RECIPE',
    payload : recipe
  }
};


// Filter  Selected Recipe
export const getSelectedRecipeFilter = (recipes, currentRecipe) => 
  (dispatch) => {
  if(typeof recipes.data.hits !== 'undefined'){
    let filtered =  recipes.data.hits.filter((rec , i) => {
        let recName = rec.recipe.label
        let currentRec = recName.toLowerCase().trim();
        currentRec = currentRec.split(' ').join('+');
        return (currentRec === currentRecipe)

    });

    dispatch(foundSelectedRecipe(filtered));
  }

};

